// ==UserScript==
// @name        Yes24 BaroOn Always
// @namespace   https://bitbucket.org/hwiorn/gm-yes24-baro-on
// @include     *yes24.com*
// @icon        http://image.yes24.com/sysimage/renew/gnb/yes24.ico
// @downloadURL https://bitbucket.org/hwiorn/gm-yes24-baro-on/raw/master/GreaseMonkey_Yes24_Baro_ON.user.js
// @updateURL   https://bitbucket.org/hwiorn/gm-yes24-baro-on/raw/master/GreaseMonkey_Yes24_Baro_ON.user.js
// @version     0.2
// @run-at      document-end
// @grant       none
// ==/UserScript==
var adult = getCookie('AdultAuthYN');
if (adult == 'undefined' || adult != 'Y') {
  setCookie('AdultAuthYN', 'Y', 1);
}
if (YesData.PID() == '') {
  //setCookie("PID","13794",1);
  document.location.href = 'http://www.yes24.com/Cooperate/Yes24Gateway.aspx?pid=94982&ReturnURL=' + encodeURIComponent(document.location.href);
}
